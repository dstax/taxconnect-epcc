# TaxConnect EPCC

## Description
This Node module provides the necessary functionality for connecting an ElasticPath Storefront to Thompson Reuters OSITD. 

## Installation
1. Sign in to the ElasticPath Commerce Cloud Manager (https://your-data-center-url)
2. Create a new Flow (https://your-data-center-url/flows) that will store your TaxConnect Credentials.
   1. New Flow
      - Name: `Onesource Config`
      - Slug: `onesource-config`
      - Description: `Variables used by Onesource for Tax Calculations`
  
    1. Create New Field
       - Name: `ENV`
       - Slug: `ENV`
       - Description: `Determines which URL to send The Tax Call to.`
       - Field Type: `string`
       - Required: `True`
       - Status: `Enabled`
       - Default: `TEST`
       - Order: `1` 
       - List: `DEV, TEST, UAT, PROD`
  
    2. Create New Field
       - Name: `API Key`
       - Slug: `API_KEY`
       - Description: `API Key to access ITBridge`
       - Field Type: `string`
       - Required: `True`
       - Status: `Enabled`
       - Order: `2` 
3. The Flow you made should now be in the Menu to the left at the buttom under the Flows section.
4. Create a new entry with the API Key and ENV you have been provided
5. We will need the entry identifier (run the script you have been provided), save the identifier we will be adding it to the Storefront config
6. Create a new ElasticPath Storefront
7. Once you have setup the EP Storefront. You will have to install this node module.
   - `yarn add https://gitlab.com/dstax/taxconnect-epcc.git`
8. Update the config.ts file (src/config), add the following line to your config.ts file
   - `OneSource_Config : process.env.OneSource_Config || 'the-entryID-goes-here-f85c1a3f1068',`
9. Update the service.ts file (src/service.ts)
   1.  Replace the `checkout` function with the following code
       ```export async function checkout(reference: string, customer: any, billing: any, shipping: any,  language: string, currency: string): Promise<{ data: moltin.Order }> {
        const moltin = MoltinGateway({ host: config.endpointURL, client_id: config.clientId, language, currency });
        console.log("Prepping data for DSTax Call")
        var cartID = reference
        console.log(cartID)
        var ship_address = new dstax.Address(shipping.postcode,shipping.country,shipping.county,shipping.city,undefined)
        var bill_address = new dstax.Address(billing.postcode,billing.country,billing.county,billing.city,undefined)
        var items = [];
        var subtotal=0;
        var cartItems =  (await getCartItems(reference)).data
        console.log(cartItems)
        // for (var i = 0; i <)
        
        // Format Items--------------------------------------------
        for (var i = 0; i < cartItems.length; i++){
            var price = cartItems[i].unit_price.amount/100
            var quantity = cartItems[i].quantity
            var sku = cartItems[i].sku
            var product_code = cartItems[i].product_id
            var discount = 0 //cartItems[i].meta.display_price.
            var item = new dstax.Item(price, quantity, sku, product_code, discount,cartItems[i].id)
            items[i] = item;
            subtotal += price * quantity
        }
        //  console.log(items)

        var call = new dstax.TaxStruct(cartID,subtotal,ship_address,bill_address,items,undefined,false,undefined,undefined,undefined)
        console.log(call)

        const slug = 'onesource-config'
        const entryId = config.OneSource_Config
        await moltin.Flows.GetEntry(slug, entryId)
        .then(entry => {
        console.log(entry)
        dstax.setEnvironment(entry.data.ENV)
        dstax.apikey(entry.data.API_KEY)
        })
        
        var result = await dstax.taxcall(call)
        console.log(result)

        
        const checkoutRes = await moltin.Cart(reference).Checkout(customer, billing, shipping);
        return checkoutRes;
        ``` 
10. When the site re-compiles, the checkout will now update the tax after your customers input their shipping and billing information.

## Support
For technical support please contact the support email below with `EPCC Support` in your subject line  
<support@dstax.com>

## License
Copyright (C) DSTAX LLC - All Rights Reserved  
Unauthorized copying of this file, via any medium is strictly prohibited  
Proprietary and confidential  
Written by Kyle Anderson <kyle.anderson@dstax.com>, February 18 2022
