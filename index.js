class DSAddress {

    constructor(postal_code = '', country = 'US', state = '', city = '', geo_code = '',) {
      this.Country = country.toUpperCase();
      this.Region = state;
      this.City = city;
      this.PostalCode = postal_code.toString();
  
      this.GeoCode = geo_code.toString();
  
      if (this.Country === 'US' && this.PostalCode.length > 5 && this.GeoCode.length === 0) {
        this.GeoCode = this.PostalCode.slice(this.PostalCode.length - 4, this.PostalCode.length);
        this.PostalCode = this.PostalCode.slice(0, 5)
      }
    };
    convert_to_bridgeFormat() {
      return JSON.stringify(this);
    }
  }
  
  class DSItem {
    /**
  * @param {any} price
  */
    constructor(price, quantity = 1, sku = '', product_code = '', discount = 0,itemid = '',) {
      this.unit_price = price;
      this.quantity = quantity;
      this.sku = sku.toString();
      this.product_code = product_code.toString();
      this.discount = discount;
      this.itemid = itemid.toString();
    };
    convert_to_bridgeFormat() {
      return JSON.stringify(this);
    }
  }
  
  class TaxStruct {
    /**
  * @param {{ toString: () => any; }} order_number
  * @param {any} subtotal
  * @param {DSAddress} ship_address
  * @param {DSAddress} bill_address
  * @param {DSAddress} ship_from_address
  * @param {DSItem | any[]} items
  */
    constructor(order_number, subtotal, ship_address, bill_address, items,
      total_shipping = 0, post_to_audit = false, customer_vat = '',
      ship_from_address = null, invoice_date = '') {
  
      this.order_number = order_number.toString();
  
      this.total_subtotal = subtotal;
      if (ship_address instanceof DSAddress && bill_address instanceof DSAddress) {
        this.addresses = {
          'ShipToAddress': ship_address,
          'BillToAddress': bill_address,
          'ShipFromAddress':ship_from_address
        };
  
      }
      // need to handle case where these aren't Address objects
      if (ship_from_address != null && ship_from_address instanceof DSAddress) { this.addresses['ShipFromAddress'] = ship_from_address; }
      // need to handle case where these aren't Address objects
  
      //console.log("\nchecking array type\n");
      if (items instanceof Array) {  //now check each item is an item objects
        //console.log("checking each item in array");
  
        for (let i = 0; i < items.length; i++) {
          if (items[i] instanceof DSItem) {
            //console.log("item ",i,  "  is of class Item");
          }
          else {
            //console.log('item ', i ," is not a class Item and should throw an error here");
          }
        };
        this.items = items;
      }
      else { //console.log ("items is not an array???");
      }
  
  
      this.total_shipping = total_shipping;
      this.post_to_audit = post_to_audit;
      this.customer_vat = customer_vat;
      //this.ship_from_address = ship_from_address;
      this.invoice_date = invoice_date;
  
  
    };
  
    convert_to_bridgeFormat() {
      return JSON.stringify(this);
    };
  }
  
  var one_source_key = '';
  var environment = '';
  /**
  * @param {string} key
  */
  function apikey(key) { one_source_key = key; };
  /**
  * @param {string} env
  */
  function setEnvironment(env) {
  
    if (env === 'DEV') { environment = 'localhost:8080'; return; }
    if (env === 'TEST') { environment = 'nexus.itbridge.dstax.com'; return; }
    if (env === 'UAT') { environment = 'uat.itbridge.dstax.com'; return; }
    if (env === 'PROD') { environment = 'dev.itbridge.dstax.com'; return; }
    console.log("environment is " + environment);
  
  };
  // function parsebody(data) { console.log( data);};
  // function stringerror(data) { console.log(data);}
  
  
  //test from ************
  
  /**
  * @param {TaxStruct} ts
  */
  async function taxcall(ts){
    const auth = "Bearer " + one_source_key;
    var url = ''
  
    if (environment === 'localhost:8080'){url = 'http://localhost:8080'}
    else{url = 'https://' + environment};
    
    var target = (url + '/elasticpath/');
    var data = ts
  
      return fetch(target, {
        body: JSON.stringify(data),
        headers: {
          'Authorization':auth,
          'dataType': 'json',
          'content-type': 'application/json',
          'Access-Control-Allow-Origin': url,
          'Access-Control-Allow-Origin': "http://localhost:3000",

  
        },
        method: 'POST',
        redirect: 'follow',
        // path: '/elasticpath/'
      })
      .then(response => {
        if (response.status === 200) {
          // let data = response.text()
          console.log("Returning Taxes");  
          return(response.json());  
          // console.log(response.json());  
      } else {
       throw new Error('Something went wrong on api server!');
      }
    })
    // .then(taxes => {
    //   console.log(taxes.TaxDetails); 
    // })
    .catch(error => {
      console.log(error);
    });
  }
  
  module.exports = { Address: DSAddress, Item: DSItem, TaxStruct, taxcall, apikey, setEnvironment };
  