// import dstax from 'dstax-node'
// declare module 'dstax-node' 
// {
//     export class Address{
//         constructor(postal_code, country, state, city, geo_code)
//     }
//     export class Item{
//         constructor(price, quantity, sku, product_code, discount)
//     }
//     export class TaxStruct{
//         constructor(order_number,  subtotal, ship_address, bill_address, items,
//             total_shipping=0, post_to_audit= false, customer_vat='',
//             ship_from_address = null, invoice_date ='')
//     }
//     export function taxcall(ts,mysuccess, myerror)
//     export function apikey(key:string):void
//     export function setEnvironment(env:string):void
// }

export class Address {
    constructor(postal_code?: string, country?: string, state?: string, city?: string, geo_code?: string);
    Country: string;
    Region: string;
    City: string;
    PostalCode: string;
    GeoCode: string;
    convert_to_bridgeFormat(): string;
}
export class Item {
    /**
  * @param {any} price
  */
    constructor(price: any, quantity?: number, sku?: string, product_code?: string, discount?: number, itemid?: string);
    unit_price: any;
    quantity: number;
    sku: string;
    product_code: string;
    discount: number;
    itemid: string;
    convert_to_bridgeFormat(): string;
}
export class TaxStruct {
    /**
  * @param {{ toString: () => any; }} order_number
  * @param {any} subtotal
  * @param {any} ship_address
  * @param {any} bill_address
  * @param {any} ship_from_address
  * @param {string | any[]} items
  */
    constructor(order_number: {
        toString: () => any;
    }, subtotal: any, ship_address: any, bill_address: any, items: string | any[], total_shipping?: number, post_to_audit?: boolean, customer_vat?: string, ship_from_address?: any, invoice_date?: string);
    order_number: any;
    total_subtotal: any;
    addresses: {
        ShipToAddress: Address;
        BillToAddress: Address;
        ShipFromAddress: any;
    };
    items: any[];
    total_shipping: number;
    post_to_audit: boolean;
    customer_vat: string;
    invoice_date: string;
    convert_to_bridgeFormat(): string;
}
/**
* @param {any} ts
*/
export function taxcall(ts: any): Promise<any>;
/**
* @param {string} key
*/
export function apikey(key: string): void;
/**
* @param {string} env
*/
export function setEnvironment(env: string): void;
